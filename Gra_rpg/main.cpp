#include <QApplication>
#include <QMessageBox>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QInputDialog>
#include <QString>
#include <QDialog>
#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

class Character {
private:
    string name;
    int strength;
    int speed;
    int agility;
    int experience;
    int level;
    int wins;
    int defeats;
    int attributePoints;


public:
    Character(const string& name, int strength, int speed, int agility)
        : name(name), strength(strength), speed(speed), agility(agility), experience(0), level(1), wins(0), defeats(0), attributePoints(0) {}

    const string& getName() const {
        return name;
    }

    int getStrength() const {
        return strength;
    }

    int getSpeed() const {
        return speed;
    }

    int getAgility() const {
        return agility;
    }

    int getExperience() const {
        return experience;
    }

    int getLevel() const {
        return level;
    }

    int getWins() const {
        return wins;
    }

    int getDefeats() const {
        return defeats;
    }

    void increaseExperience(int amount) {
        experience += amount;
    }



    void increaseWins() {
        wins++;
    }

    void increaseWins(int value) {
        wins=value;
    }

    void increaseDefeats() {
        defeats++;
    }

    void increaseDefeats(int value) {
        defeats=value;
    }

    int getAttributePoints() const {
        return attributePoints;
    }

    void increaseAttribute(const string& attributeName, int value) {
        if (attributePoints >= value) {
            if (attributeName == "strength") {
                strength += value;
            } else if (attributeName == "speed") {
                speed += value;
            } else if (attributeName == "agility") {
                agility += value;
            }
            attributePoints -= value;
        }
    }

    void increaseLevel() {
        level++;
        attributePoints++; // dodatkowy punkt atrybutów przy zwiększaniu poziomu
    }

    void increaseLevel(int value) {
        level=value;

    }
};

class Enemy {
private:
    string name;
    int strength;
    int speed;
    int agility;

public:
    Enemy() {
        generateEnemy();
    }

    const string& getName() const {
        return name;
    }

    int getStrength() const {
        return strength;
    }

    int getSpeed() const {
        return speed;
    }

    int getAgility() const {
        return agility;
    }

private:
    void generateEnemy() {
        strength = rand() % 8;
        speed = rand() % 8;
        agility = rand() % 8;

        string names[] = { "Wraith", "Cyclops", "Harpy", "Minotaur", "Banshee", "Ogre", "Demon", "Specter" };
        int numNames = sizeof(names) / sizeof(names[0]);
        int randomIndex = rand() % numNames;
        name = names[randomIndex];
    }
};

bool isCharacterExist(const vector<Character>& characters, const string& name) {
    for (const Character& character : characters) {
        if (character.getName() == name) {
            return true;
        }
    }
    return false;
}

vector<Character> characters;

int calculateFightResult(const Character& player, const Enemy& enemy) {
    int playerScore = player.getStrength() + player.getSpeed() + player.getAgility();
    int enemyScore = enemy.getStrength() + enemy.getSpeed() + enemy.getAgility();

    // Dodanie elementu losowości
    int playerRandomFactor = rand() % 10;
    int enemyRandomFactor = rand() % 10;

    playerScore += playerRandomFactor;
    enemyScore += enemyRandomFactor;

    if (playerScore > enemyScore) {
        return 1; // Wygrana gracza
    } else if (playerScore < enemyScore) {
        return -1; // Przegrana gracza
    } else {
        return 0; // Remis
    }
}

class FightResultDialog : public QDialog {
public:
    FightResultDialog(Character& player, const Enemy& enemy) {
        QVBoxLayout* layout = new QVBoxLayout();

        QLabel* titleLabel = new QLabel("Wynik walki");
        titleLabel->setAlignment(Qt::AlignCenter);
        layout->addWidget(titleLabel);

        QLabel* playerLabel = new QLabel(QString::fromStdString("Postać: " + player.getName()));
        layout->addWidget(playerLabel);

        QLabel* playerStrengthLabel = new QLabel(QString::fromStdString("Siła: " + to_string(player.getStrength())));
        layout->addWidget(playerStrengthLabel);

        QLabel* playerSpeedLabel = new QLabel(QString::fromStdString("Szybkość: " + to_string(player.getSpeed())));
        layout->addWidget(playerSpeedLabel);

        QLabel* playerAgilityLabel = new QLabel(QString::fromStdString("Zwinność: " + to_string(player.getAgility())));
        layout->addWidget(playerAgilityLabel);

        QLabel* enemyLabel = new QLabel(QString::fromStdString("Przeciwnik: " + enemy.getName()));
        layout->addWidget(enemyLabel);

        QLabel* enemyStrengthLabel = new QLabel(QString::fromStdString("Siła: " + to_string(enemy.getStrength())));
        layout->addWidget(enemyStrengthLabel);

        QLabel* enemySpeedLabel = new QLabel(QString::fromStdString("Szybkość: " + to_string(enemy.getSpeed())));
        layout->addWidget(enemySpeedLabel);

        QLabel* enemyAgilityLabel = new QLabel(QString::fromStdString("Zwinność: " + to_string(enemy.getAgility())));
        layout->addWidget(enemyAgilityLabel);

        QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
        layout->addWidget(buttonBox);
        QObject::connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

        setLayout(layout);
        setWindowTitle("Wynik walki");

        int fightResult = calculateFightResult(player, enemy);
        if (fightResult == 1) {
            player.increaseExperience(50);
            player.increaseWins();
            QLabel* resultLabel = new QLabel("Wynik walki: Wygrana");
            layout->addWidget(resultLabel);
        } else if (fightResult == -1) {
            player.increaseExperience(25);
            player.increaseDefeats();
            QLabel* resultLabel = new QLabel("Wynik walki: Przegrana");
            layout->addWidget(resultLabel);
        } else {
            QLabel* resultLabel = new QLabel("Wynik walki: Remis");
            layout->addWidget(resultLabel);
        }
    }
};

class AllCharactersAttributesDialog : public QDialog {
public:
    AllCharactersAttributesDialog(const vector<Character>& characters) {
        QVBoxLayout* layout = new QVBoxLayout();

        QLabel* titleLabel = new QLabel("Atrybuty wszystkich postaci");
        titleLabel->setAlignment(Qt::AlignCenter);
        layout->addWidget(titleLabel);

        for (const Character& character : characters) {
            QLabel* nameLabel = new QLabel(QString::fromStdString("Nazwa: " + character.getName()));
            layout->addWidget(nameLabel);

            QLabel* strengthLabel = new QLabel(QString::fromStdString("Siła: " + to_string(character.getStrength())));
            layout->addWidget(strengthLabel);

            QLabel* speedLabel = new QLabel(QString::fromStdString("Szybkość: " + to_string(character.getSpeed())));
            layout->addWidget(speedLabel);

            QLabel* agilityLabel = new QLabel(QString::fromStdString("Zwinność: " + to_string(character.getAgility())));
            layout->addWidget(agilityLabel);

            QLabel* expLabel = new QLabel(QString::fromStdString("Doświadczenie: " + to_string(character.getExperience())));
            layout->addWidget(expLabel);

            QLabel* levelLabel = new QLabel(QString::fromStdString("Poziom: " + to_string(character.getLevel())));
            layout->addWidget(levelLabel);

            QLabel* winsLabel = new QLabel(QString::fromStdString("Wygrane: " + to_string(character.getWins())));
            layout->addWidget(winsLabel);

            QLabel* defeatsLabel = new QLabel(QString::fromStdString("Porażki: " + to_string(character.getDefeats())));
            layout->addWidget(defeatsLabel);

            QLabel* separatorLabel = new QLabel("----------------------------");
            layout->addWidget(separatorLabel);
        }

        QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
        layout->addWidget(buttonBox);
        QObject::connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

        setLayout(layout);
        setWindowTitle("Atrybuty wszystkich postaci");
    }
};

void createNewCharacter() {
    QString name;
    int strength, speed, agility;
    bool ok;

    name = QInputDialog::getText(nullptr, "Nowa postać", "Wprowadź nazwę postaci:");

    while (isCharacterExist(characters, name.toStdString())) {
        name = QInputDialog::getText(nullptr, "Nowa postać", "Postać o podanej nazwie już istnieje. Wprowadź inną nazwę:");
    }

    int availablePoints = 10;

    do {
        strength = QInputDialog::getInt(nullptr, "Nowa postać", "Dostępne punkty: " + QString::number(availablePoints) + "\nWprowadź punkty siły:", 0, 0, availablePoints, 1, &ok);

        if (ok) {
            availablePoints -= strength;

            if (availablePoints < 0) {
                QMessageBox::critical(nullptr, "Błąd", "Suma punktów przekracza dostępną liczbę punktów.");
                ok = false;
            }
        }
        else {
            QMessageBox::critical(nullptr, "Błąd", "Wprowadzona wartość musi być liczbą całkowitą.");
        }
    } while (!ok);

    availablePoints = 10 - strength;

    do {
        speed = QInputDialog::getInt(nullptr, "Nowa postać", "Dostępne punkty: " + QString::number(availablePoints) + "\nWprowadź punkty szybkości:", 0, 0, availablePoints, 1, &ok);

        if (ok) {
            availablePoints -= speed;

            if (availablePoints < 0) {
                QMessageBox::critical(nullptr, "Błąd", "Suma punktów przekracza dostępną liczbę punktów.");
                ok = false;
            }
        }
        else {
            QMessageBox::critical(nullptr, "Błąd", "Wprowadzona wartość musi być liczbą całkowitą.");
        }
    } while (!ok);

    agility = availablePoints;

    characters.push_back(Character(name.toStdString(), strength, speed, agility));
    QMessageBox::information(nullptr, "Sukces", "Nowa postać została utworzona.");
}

void increaseLevel(Character& character) {
    int experienceThreshold = character.getLevel() * 100;
    if (character.getExperience() >= experienceThreshold) {
        character.increaseLevel();
        cout << "Gratulacje! Zwiększyłeś poziom. Twój obecny poziom to: " << character.getLevel() << endl;
        character.increaseExperience(0);

        // Rozdysponowanie punktu atrybutów po zwiększeniu poziomu
        int attributePoints = character.getAttributePoints();
        if (attributePoints > 0) {
            QMessageBox::information(nullptr, "Nowy poziom", "Otrzymałeś dodatkowy punkt atrybutów. Możesz go rozdysponować.");

            bool ok;
            QString attributeName = QInputDialog::getItem(nullptr, "Rozdysponowanie atrybutu", "Wybierz atrybut:", QStringList() << "strength" << "speed" << "agility", 0, false, &ok);
            if (ok) {
                int attributeValue = QInputDialog::getInt(nullptr, "Rozdysponowanie atrybutu", "Podaj wartość atrybutu:", 0, 0, attributePoints, 1, &ok);
                if (ok) {
                    character.increaseAttribute(attributeName.toStdString(), attributeValue);
                }
            }
        }
    }
}

void fight() {
    if (characters.empty()) {
        QMessageBox::critical(nullptr, "Błąd", "Brak dostępnych postaci. Utwórz nową postać.");
        return;
    }

    int playerIndex = QInputDialog::getInt(nullptr, "Walka", "Wybierz numer postaci gracza:", 1, 1, characters.size(), 1);
    Character& player = characters[playerIndex - 1];

    Enemy enemy;

    FightResultDialog* dialog = new FightResultDialog(player, enemy);
    dialog->exec();

    increaseLevel(player);
}


void displayCharacters() {
    QDialog* dialog = new AllCharactersAttributesDialog(characters);
    dialog->exec();
}

void saveProgress(const vector<Character>& characters) {
    ofstream file("save.txt");
    if (!file) {
        QMessageBox::critical(nullptr, "Błąd", "Nie udało się zapisać postępów.");
        return;
    }

    for (const Character& character : characters) {
        file << character.getName() << " "
             << character.getStrength() << " "
             << character.getSpeed() << " "
             << character.getAgility() << " "
             << character.getExperience() << " "
             << character.getLevel() << " "
             << character.getWins() << " "
             << character.getDefeats() << " "
             << character.getAttributePoints() << endl;
    }

    file.close();
    QMessageBox::information(nullptr, "Sukces", "Postępy zostały zapisane.");
}

void loadProgress(vector<Character>& characters) {
    ifstream file("save.txt");
    if (!file) {
        QMessageBox::critical(nullptr, "Błąd", "Nie udało się wczytać postępów.");
        return;
    }

    characters.clear();
    string name;
    int strength, speed, agility, experience, level, wins, defeats, attributePoints;

    while (file >> name >> strength >> speed >> agility >> experience >> level >> wins >> defeats >> attributePoints) {
        characters.push_back(Character(name, strength, speed, agility));
        Character& character = characters.back();
        character.increaseExperience(experience);
        character.increaseLevel(level);
        character.increaseWins(wins);
        character.increaseDefeats(defeats);


    }

    file.close();
    QMessageBox::information(nullptr, "Sukces", "Postępy zostały wczytane.");
}

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    QWidget window;
    window.setWindowTitle("Menu");

    QVBoxLayout* layout = new QVBoxLayout();

    QLabel* titleLabel = new QLabel("======= MENU =======");
    layout->addWidget(titleLabel);

    QPushButton* createButton = new QPushButton("Stwórz nową postać");
    layout->addWidget(createButton);
    QObject::connect(createButton, &QPushButton::clicked, createNewCharacter);

    QPushButton* fightButton = new QPushButton("Prowadź walkę");
    layout->addWidget(fightButton);
    QObject::connect(fightButton, &QPushButton::clicked, fight);

    QPushButton* displayAllButton = new QPushButton("Wyświetl wszystkie postacie");
    layout->addWidget(displayAllButton);
    QObject::connect(displayAllButton, &QPushButton::clicked, displayCharacters);

    QPushButton* saveButton = new QPushButton("Zapisz postępy");
                              layout->addWidget(saveButton);
    QObject::connect(saveButton, &QPushButton::clicked, [&characters]() { saveProgress(characters); });

    QPushButton* loadButton = new QPushButton("Wczytaj postępy");
                              layout->addWidget(loadButton);
    QObject::connect(loadButton, &QPushButton::clicked, [&characters]() { loadProgress(characters); });

    QPushButton* exitButton = new QPushButton("Wyjdź");
    layout->addWidget(exitButton);
    QObject::connect(exitButton, &QPushButton::clicked, &app, &QApplication::quit);

    window.setLayout(layout);
    window.show();

    return app.exec();
}
