#include <iostream>
#include <locale>
#include <ctime>
#include <vector>
using namespace std;

//Struktura postaci
struct Character {
    string name;
    int strength;
    int speed;
    int agility;
    int experience;
    int level;
    int wins;
    int defeat;
};

//Tworzenie postaci
void createNewCharacter(vector<Character>& characters) {
    Character character;
    cout << "Wprowad� nazw� postaci: ";
    getline(cin, character.name);

    while (isCharacterExist(characters, character.name)) {
        cout << "Posta� o podanej nazwie ju� istnieje. Wprowad� inn� nazw�: ";
        getline(cin, character.name);
    }

    int availablePoints = 10;
    do {
    cout << "Dost�pne punkty: " << availablePoints << endl;
    cout << "Wprowad� punkty si�y: ";
    cin >> character.strength;

    cout << "Wprowad� punkty szybko�ci: ";
    cin >> character.speed;

    cout << "Wprowad� punkty zwinno�ci: ";
    cin >> character.agility;

    int totalPoints = character.strength + character.speed + character.agility;

        if (totalPoints > availablePoints) {
            cout << "Przekroczono liczb� dost�pnych punkt�w! Spr�buj ponownie." <<endl;
        } else {
            break;
        }
    } while (true);

    character.experience = 0;
    character.level = 1;
    character.wins=0;
    character.defeat=0;

    characters.push_back(character);

    cout << "Posta� zosta�a utworzona!" << endl;
}

//Losowanie przeciwnika
void generateEnemy(Character* enemy) {
    // Losowe cechy postaci z przedzia�u 0-5
    enemy->strength = rand() % 8;
    enemy->speed = rand() % 8;
    enemy->agility = rand() % 8;

    // Losowa nazwa przeciwnika
    string names[] = { "Wraith", "Cyclops", "Harpy", "Minotaur", "Banshee", "Ogre", "Demon", "Specter" };
    int numNames = sizeof(names) / sizeof(names[0]);
    int randomIndex = rand() % numNames;
    enemy->name = names[randomIndex];
}


//Walka
void fight(Character* player, const Character& enemy) {
    cout << "Posta�: " << player->name << endl;
    cout << "Si�a: " << player->strength << endl;
    cout << "Szybko��: " << player->speed << endl;
    cout << "Zwinno��: " << player->agility << endl;
    cout << "Poziom: " << player->level << endl;
    cout << "Do�wiadczenie: " << player->experience << endl;
    cout << "Wygrane: " << player->wins << endl;
    cout << "Pora�ki: " << player->defeat << endl;

    cout << "Przeciwnik: " << enemy.name << endl;
    cout << "Si�a: " << enemy.strength << endl;
    cout << "Szybko��: " << enemy.speed << endl;
    cout << "Zwinno��: " << enemy.agility << endl;

    // Por�wnanie atrybut�w gracza i przeciwnika
    int playerTotal = player->strength + player->speed + player->agility;
    int enemyTotal = enemy.strength + enemy.speed + enemy.agility;

    if (playerTotal > enemyTotal) {
        cout << "Wygrana!" << endl;
        player->wins += 1;
        player->experience += 50;
    } else if (playerTotal < enemyTotal) {
        cout << "Pora�ka!" << endl;
        player->defeat += 1;
        player->experience += 10;
    } else {
        cout << "Remis!" << endl;
        player->experience += 15;
    }

    increaseLevel(player); // Sprawdzenie i zwi�kszenie poziomu gracza po walce
}

//Sprawdzenie czy wprowadzona nazwa ju� istnieje
bool isCharacterExist(const vector<Character>& characters, const string& name) {
    for (const Character& character : characters) {
        if (character.name == name) {
            return true;
        }
    }
    return false;
}

// Zwi�kszenie poziomu postaci na podstawie zdobytego do�wiadczenia
void increaseLevel(Character* character) {
    int experienceThreshold = character->level * 100; // Pr�g do�wiadczenia, kt�ry musi by� przekroczony dla ka�dego poziomu
    if (character->experience >= experienceThreshold) {
        character->level += 1;
        cout << "Gratulacje! Zwi�kszy�e� poziom. Tw�j obecny poziom to: " << character->level << endl;
        character->experience = 0; // Resetowanie do�wiadczenia po zdobyciu nowego poziomu
    }
}

//Wy�wietlanie informacji o postaci
void displayCharacter(const Character& character) {
    cout << "Posta�: " << character.name << endl;
    cout << "Si�a: " << character.strength << endl;
    cout << "Szybko��: " << character.speed << endl;
    cout << "Zwinno��: " << character.agility << endl;
    cout << "Poziom: " << character.level << endl;
    cout << "Do�wiadczenie: " << character.experience << endl;
    cout << "Wygrane: " << character.wins << endl;
    cout << "Pora�ki: " << character.defeat << endl;
}

// Wy�wietlanie menu
void displayMenu() {
    cout << "======= MENU =======" << endl;
    cout << "1. Stw�rz now� posta�" << endl;
    cout << "2. Prowad� walk�" << endl;
    cout << "3. Wy�wietl posta�" << endl;
    cout << "4. Zako�cz program" << endl;
    cout << "Wybierz opcj�: ";
}

// Wy�wietlanie postaci dost�pnych do walki
void displayCharacters(const vector<Character>& characters) {
    cout << "Dost�pne postacie: " << endl;
    for (int i = 0; i < characters.size(); i++) {
        cout << i + 1 << ". " << characters[i].name << endl;
    }
}

int main() {
    setlocale(LC_ALL,"polish");
    srand(static_cast<unsigned int>(time(NULL)));

    vector<Character> characters;
    int choice;

    do {
        displayMenu();
        cin >> choice;
        cin.ignore();

        switch (choice) {
            case 1:
                createNewCharacter(characters);
                break;
            case 2:
                if (characters.empty()) {
                    cout << "Nie masz �adnej postaci. Stw�rz najpierw now� posta�." << endl;
                } else {
                    displayCharacters(characters);
                    int characterIndex;
                    cout << "Wybierz posta� do walki: ";
                    cin >> characterIndex;
                    cin.ignore();

                    if (characterIndex > 0 && characterIndex <= characters.size()) {
                        Character player = characters[characterIndex - 1];
                        Character enemy;
                        generateEnemy(&enemy);
                        fight(&player, enemy);
                        characters[characterIndex - 1] = player;  // Aktualizacja postaci w wektorze po walce
                    } else {
                        cout << "Nieprawid�owy wyb�r postaci." << endl;
                    }
                }
                break;
            case 3:
                if (characters.empty()) {
                    cout << "Nie masz �adnej postaci. Stw�rz najpierw now� posta�." << endl;
                } else {
                    displayCharacters(characters);
                    int characterIndex;
                    cout << "Wybierz posta� do wy�wietlenia: ";
                    cin >> characterIndex;
                    cin.ignore();

                    if (characterIndex > 0 && characterIndex <= characters.size()) {
                        Character player = characters[characterIndex - 1];
                        displayCharacter(player);
                    } else {
                        cout << "Nieprawid�owy wyb�r postaci." << endl;
                    }
                }
                break;
            case 4:
                cout << "Koniec programu." << endl;
                break;
            default:
                cout << "Nieprawid�owa opcja. Wybierz ponownie." << endl;
                break;
        }

        cout << endl;

    } while (choice != 4);

    return 0;
}
